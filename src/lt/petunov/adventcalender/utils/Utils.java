package lt.petunov.adventcalender.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Utils {

    public static List<String> readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
