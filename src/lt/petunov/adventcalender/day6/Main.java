package lt.petunov.adventcalender.day6;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    private static final String TURN_ON = "turn on";
    private static final String TURN_OFF = "turn off";
    private static final String TOGGLE = "toggle";
    private static final String THROUGH = "through";

    public static void main(String[] args) {
        int[] result = countLamps(Utils.readFile("input/day6/input.txt"));
        System.out.println("Turned on lamps " + result[0] + ". Total brightness is " + result[1] + ".");
    }

    private static int[] countLamps(List<String> commands) {
        boolean[][] simpleLamps = new boolean[1000][1000];
        int[][] dimmingLamps = new int[1000][1000];

        for (String command : commands) {
            Pattern pattern = Pattern.compile("(\\D+)\\s(\\d+),(\\d+)\\s" + THROUGH + "\\s(\\d+),(\\d+)");
            Matcher m = pattern.matcher(command);
            if (m.find()) {
                String instruction = m.group(1);
                int x0 = Integer.parseInt(m.group(2));
                int y0 = Integer.parseInt(m.group(3));
                int x1 = Integer.parseInt(m.group(4));
                int y1 = Integer.parseInt(m.group(5));

                executeSimpleInstruction(simpleLamps, instruction, x0, y0, x1, y1);
                executeDimmingInstruction(dimmingLamps, instruction, x0, y0, x1, y1);
            }
        }

        return new int[] { calculateTurnedOnLamps(simpleLamps), calculateBrightness(dimmingLamps) };
    }


    private static int calculateBrightness(int[][] lamps) {
        int result = 0;

        for (int i = 0; i < lamps.length; i++) {
            for (int j = 0; j < lamps[i].length; j++) {
                result += lamps[i][j];
            }
        }

        return result;
    }

    private static int calculateTurnedOnLamps(boolean[][] lamps) {
        int result = 0;

        for (int i = 0; i < lamps.length; i++) {
            for (int j = 0; j < lamps[i].length; j++) {
                if (lamps[i][j])
                    result++;
            }
        }

        return result;
    }

    private static void executeDimmingInstruction(int[][] lamps, String instruction, int x0, int y0, int x1,
            int y1) {
        for (int i = x0; i <= x1; i++) {
            for (int j = y0; j <= y1; j++) {
                switch (instruction) {
                    case TOGGLE:
                        lamps[i][j] += 2;
                        break;
                    case TURN_ON:
                        lamps[i][j]++;
                        break;
                    case TURN_OFF:
                        if (lamps[i][j] > 0)
                            lamps[i][j]--;
                        break;
                }
            }
        }
    }

    private static void executeSimpleInstruction(boolean[][] lamps, String instruction, int x0, int y0, int x1,
            int y1) {
        for (int i = x0; i <= x1; i++) {
            for (int j = y0; j <= y1; j++) {
                switch (instruction) {
                    case TOGGLE:
                        lamps[i][j] = !lamps[i][j];
                        break;
                    case TURN_ON:
                        lamps[i][j] = true;
                        break;
                    case TURN_OFF:
                        lamps[i][j] = false;
                        break;
                }
            }
        }

    }

}
