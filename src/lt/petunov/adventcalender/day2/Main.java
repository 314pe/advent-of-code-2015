package lt.petunov.adventcalender.day2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Testing scenarios
        // System.out.println("calculate paper for " + calculatePaper("2x3x4"));
        // System.out.println("calculate ribbon for " + calculateRibbon("2x3x4"));
        //
        // System.out.println("calculate paper for " + calculatePaper("1x1x10"));
        // System.out.println("calculate ribbon for " + calculateRibbon("1x1x10"));

        System.out.println("Calculate total paper for " + calculateTotalPaper(readFile("input/day2/input.txt")));
        System.out.println("Calculate total ribbon for " + calculateTotalRibbon(readFile("input/day2/input.txt")));

    }

    private static int calculateRibbon(String present) {
        String[] dimensions = present.split("x");
        int l = Integer.parseInt(dimensions[0]);
        int w = Integer.parseInt(dimensions[1]);
        int h = Integer.parseInt(dimensions[2]);

        int ribbon = 0;
        if (l >= w && l >= h) {
            ribbon = 2 * w + 2 * h;
        } else if (w >= l && w >= h) {
            ribbon = 2 * l + 2 * h;
        } else {
            ribbon = 2 * w + 2 * l;
        }

        ribbon += l * w * h;

        return ribbon;
    }

    private static int calculatePaper(String present) {
        String[] dimensions = present.split("x");
        int l = Integer.parseInt(dimensions[0]);
        int w = Integer.parseInt(dimensions[1]);
        int h = Integer.parseInt(dimensions[2]);

        // all three sides of a box
        int a = l * w;
        int b = w * h;
        int c = h * l;

        int smallestSide = min(a, b, c);

        return 2 * l * w + 2 * w * h + 2 * h * l + smallestSide;
    }

    private static Integer calculateTotalRibbon(List<String> presents) {
        Integer result = 0;

        for (String present : presents) {
            result += calculateRibbon(present);
        }

        return result;
    }

    private static Integer calculateTotalPaper(List<String> presents) {
        Integer result = 0;

        for (String present : presents) {
            result += calculatePaper(present);
        }

        return result;
    }

    private static List<String> readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int min(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

}
