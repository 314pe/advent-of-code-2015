package lt.petunov.adventcalender.day5;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lt.petunov.adventcalender.utils.Utils;

public class Main {

    public static void main(String[] args) {
        int[] words = countNiceWords(Utils.readFile("input/day5/input.txt"));
        System.out.println("File contains " + words[0] + " nice words and " + words[1] + " realy nice words.");

    }

    private static int[] countNiceWords(List<String> words) {
        int niceWords = 0;
        int realyNiceWords = 0;

        for (String word : words) {
            if (isItNice(word))
                niceWords++;
            if (isItReallyNice(word))
                realyNiceWords++;
        }

        return new int[] { niceWords, realyNiceWords };
    }

    // part2
    private static boolean isItReallyNice(String word) {
        // Contains a pair of letters appearing twice
        Pattern pattern = Pattern.compile("([a-z]{2}).*\\1");
        Matcher m = pattern.matcher(word);
        if (!m.find()) {
            return false;
        }

        // Contains letter with something in between
        pattern = Pattern.compile("(\\w)\\w\\1");
        m = pattern.matcher(word);
        if (!m.find()) {
            return false;
        }

        return true;
    }

    // part1
    private static boolean isItNice(String word) {
        // Contains aeiou letters at least 3 times
        Pattern pattern = Pattern.compile("([aeiou].*){3}");
        Matcher m = pattern.matcher(word);
        if (!m.find()) {
            return false;
        }

        // Contains letter twice
        pattern = Pattern.compile("(\\w)\\1");
        m = pattern.matcher(word);
        if (!m.find()) {
            return false;
        }

        // Does not contain ab, cd, pq, or xy
        pattern = Pattern.compile("(ab|cd|pq|xy)");
        m = pattern.matcher(word);
        if (m.find()) {
            return false;
        }
        
        return true;
    }

}
