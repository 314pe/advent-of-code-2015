package lt.petunov.adventcalender.day1;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    // (()) and ()() both result in floor 0.
    // ((( and (()(()( both result in floor 3.
    // ))((((( also results in floor 3.
    // ()) and ))( both result in floor -1 (the first basement level).
    // ))) and )())()) both result in floor -3.

    public static void main(String[] args) {

        System.out.println("Get final floor number " + getFloorNumber("(())"));
        System.out.println("Get final floor number " + getFloorNumber("()()"));
        System.out.println("Get final floor number " + getFloorNumber("((("));
        System.out.println("Get final floor number " + getFloorNumber("(()(()("));
        System.out.println("Get final floor number " + getFloorNumber("))((((("));
        System.out.println("Get final floor number " + getFloorNumber("())"));
        System.out.println("Get final floor number " + getFloorNumber("))("));
        System.out.println("Get final floor number " + getFloorNumber(")))"));
        System.out.println("Get final floor number " + getFloorNumber(")())())"));
        System.out.println("Get final floor number " + getFloorNumber(readFile("input/day1/input.txt")));
    }

    private static String readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            // Only first line is needed
            return lines.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int getFloorNumber(String code) {
        int floor = 0;
        boolean reachedBasement = false;

        String[] commands = code.split("");
        for (int i = 0; i < commands.length; i++) {
            String command = commands[i];
            if (command.equals("("))
                floor++;
            else
                floor--;

            if (floor == -1 && !reachedBasement) {
                reachedBasement = true;
                System.out.println("Reached basement on command " + (i + 1));
            }
        }

        return floor;
    }

}
