package lt.petunov.adventcalender.day4;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Main {

    public static void main(String[] args) {
        String key = "iwrupvqb";
        System.out.println("coin for key " + key + " is " + getPassword(key, "00000"));
        System.out.println("advanced coin for key " + key + " is " + getPassword(key, "000000"));
    }

    private static Integer getPassword(String doorId, String pattern) {
        Integer i = 0;
        while (true) {
            String md5 = "";
            String key = doorId + i;
            MessageDigest md;
            try {
                md = MessageDigest.getInstance("MD5");
                md5 = DatatypeConverter.printHexBinary(md.digest(key.getBytes()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            if (md5.startsWith(pattern)) {
                return i;
            }
            i++;
        }
    }

}
