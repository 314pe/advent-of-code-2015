package lt.petunov.adventcalender.day3;

import java.awt.Point;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        // Testing scenarios
        // System.out.println("Houses visited " + calculateVisitedHouses(">"));
        // System.out.println("Houses visited " + calculateVisitedHouses("^>v<"));
        // System.out.println("Houses visited " + calculateVisitedHouses("^v^v^v^v^v"));

        System.out.println("Houses visited " + calculateVisitedHouses(readFile("input/day3/input.txt")));
        System.out.println(
                "Houses visited with robo santa " + calculateVisitedHousesWithRobo(readFile("input/day3/input.txt")));
    }

    private static int calculateVisitedHousesWithRobo(String course) {
        Set<Point> houses = new HashSet<Point>();

        int santaX = 0;
        int santaY = 0;
        int roboX = 0;
        int roboY = 0;
        houses.add(new Point(santaX, santaY));
        String[] commands = course.split("");
        for (int i = 0; i < commands.length; i++) {
            String command = commands[i];

            switch (command) {
                case "^":
                    if (i % 2 == 0)
                        santaY++;
                    else
                        roboY++;
                    break;
                case "v":
                    if (i % 2 == 0)
                        santaY--;
                    else
                        roboY--;
                    break;
                case ">":
                    if (i % 2 == 0)
                        santaX++;
                    else
                        roboX++;
                    break;
                case "<":
                    if (i % 2 == 0)
                        santaX--;
                    else
                        roboX--;
                    break;
            }

            houses.add(new Point(santaX, santaY));
            houses.add(new Point(roboX, roboY));
        }

        return houses.size();
    }

    private static int calculateVisitedHouses(String course) {
        Set<Point> houses = new HashSet<Point>();

        int x = 0;
        int y = 0;
        houses.add(new Point(x, y));
        for (String command : course.split("")) {
            switch (command) {
                case "^":
                    y++;
                    break;
                case "v":
                    y--;
                    break;
                case ">":
                    x++;
                    break;
                case "<":
                    x--;
                    break;
            }

            houses.add(new Point(x, y));
        }

        return houses.size();
    }

    private static String readFile(String path) {
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
            return lines.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
